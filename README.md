# no_game_no_life

#### 介绍
三个像素游戏,贪吃蛇,推箱子,俄罗斯方块,有屏就能玩,移植方便

#### 软件架构
软件架构说明


#### 安装教程

1.  复制粘粘即可，cpp不管他，如果你用的easyx图形库，那么你就要用cpp,如果不是，则.c文件即可
2.  需修改的文件为key_dis.cpp主要是显示和键盘的接口
3.  可以在key_dis.h中修改ui的长宽像素大小。

#### 使用说明

1.  wasd移动，q贪吃蛇加速和箱子复位 esc退出
2.  请关闭大小写
3.  俄罗斯方块也是wasd。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

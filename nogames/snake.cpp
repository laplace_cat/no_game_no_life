/*
 * sank.cpp
 *
 *  Created on: 2022年4月6日
 *      Author: laplace
 * WASD 上下左右
 * Q加速，死了就重开
 */
 //extern "C"
#include "string.h"
#include "key_dis.h"
#ifdef PC_GAME
#include <iostream>
#endif

//蛇身体，x，y为每一节坐标，next指向后面的一节身体，before指向前面一节身体
typedef struct _snake_body {
	int x;
	int y;
	struct _snake_body* next;
	struct _snake_body* before;
}snake_body_t;
//蛇链表结构体
typedef struct _snake_list {
	snake_body_t* head;	//蛇头指针，更改值会找不到尾巴
	snake_body_t* tail;	//蛇尾巴，当作变量使用，运行find_tail()后就是尾巴
	char width;			//蛇宽度
	int driction;		//蛇运动朝向
	int len;			//蛇长度节
}snake_t;
//蛋结构体
typedef struct egg {
	int x;
	int y;
	char size;
}egg_t;

//初始化蛇吃的蛋，随机坐标
void create_egg(egg_t* e, int width) {
	e->size = width;
	e->x = rand() % GAME_RANGEX;
	e->y = rand() % GAME_RANGEY;

}
//创建长度为len，宽为width的蛇，初始头坐标为x，y
snake_t* create_snake(int len, int x, int y, int width)
{
	snake_t* s = (snake_t*)malloc(sizeof(snake_t) + 1);
	snake_body_t* head;
	snake_body_t* p;
	head = (snake_body_t*)malloc(sizeof(snake_body_t) * (len + 1));
	s->len = len;
	s->head = head;
	s->width = width;
	s->driction = GAME_KEY_W;
	int i = 0;
	p = head;
	s->driction = 1;
	for (; i < s->len; i++)
	{
		p->x = x - i;
		p->y = y;
		p->next = p + 1;
		p = p->next;
		p->before = p - 1;
	}
	return s;
}

snake_body_t* find_head(snake_t* s)
{
	return s->head;
}
//根据蛇头找到蛇尾
snake_body_t* find_tail(snake_t* s)
{
	snake_body_t* tail = s->head;
	int i = 0;
	for (; i < s->len - 1; i++)
	{
		tail = tail->next;
	}
	s->tail = tail;
	return tail;
}
//给蛇加len的长度
snake_t* add_snake_len(snake_t* s, int add_len)
{
	find_tail(s);
	snake_t* add_s = create_snake(s->tail->x, s->tail->y, add_len, s->width);
	s->tail->next = add_s->head;
	add_s->head->before = s->tail;
	s->len += add_len;
	return s;
}
//清除内存
void clear_snake(snake_t* s)
{
	//snake_body_t* b = find_tail(s);
	//snake_body_t* temp;
	free(s->head);
	free(s);
	//while (b->before!= NULL)
	//{
	//	temp = b->before;
	//	free(b);
	//	b = temp;
	//}
}
void place_egg(egg_t* e)
{
	draw_circle(e->x, e->y);
}
//画蛇
void draw_snake(snake_t* s)
{
	snake_body_t* b = s->head;
	int i = 0;
	for (; i < s->len; i++)
	{
		draw_cube(b->x, b->y);
		//printf("xy:%d,%d\n",b->x,b->y);
		b = b->next;
	}
}
//&& (p->y < e->y - e->size)
//判断是否咬到自己
char if_die(snake_t* s)
{
	snake_body_t* p;
	int i = 0;
	p = find_tail(s);
	for (; i < s->len - 2; i++)
	{
		if ((s->head->x == p->x) && (s->head->y == p->y))return 1;
		p = p->before;
	}
	return 0;
}
//判断是否吃到蛋
char if_eat_egg(egg_t* e, snake_t* s)
{
	int i = 0;
	snake_body_t* p = s->head;
	for (; i < s->len - 1; i++) {
		if ((p->x == e->x) && (e->y == p->y)) {
			add_snake_len(s, 2);
			create_egg(e, s->width);
			p = p->next;
			return 1;
		}
	}
	return 0;
}
//蛇向driction移动一格
void snake_go(snake_t* s)
{
	static int last_key;
	int i = 0;
	find_tail(s);
	//限制蛇的范围
	if (s->head->x >= GAME_RANGEX)s->head->x = 0;
	if (s->head->x < 0)s->head->x = GAME_RANGEX;
	if (s->head->y >= GAME_RANGEY)s->head->y = 0;
	if (s->head->y < 0)s->head->y = GAME_RANGEY;
	//将蛇的每一格都向前移动到前一格
	for (; i < s->len - 1; i++)
	{
		s->tail->x = s->tail->before->x;
		s->tail->y = s->tail->before->y;
		s->tail = s->tail->before;
	}
	//将蛇头向driction移动
	switch (s->driction)
	{
	case GAME_KEY_W:	 s->head->y = s->head->y - s->width; break;
	case GAME_KEY_S:s->head->y = s->head->y + s->width; break;
	case GAME_KEY_D:s->head->x = s->head->x + s->width; break;
	case GAME_KEY_A:s->head->x = s->head->x - s->width; break;
	}
}
int snake_key(int* key, int* ms)
{
	static int last_key = GAME_KEY_W;
	if ((*key == GAME_KEY_W && last_key != GAME_KEY_S) || (*key == GAME_KEY_S && last_key != GAME_KEY_W)
		|| (*key == GAME_KEY_A && last_key != GAME_KEY_D) || (*key == GAME_KEY_D && last_key != GAME_KEY_A))last_key = *key;
	if (*key == GAME_Q) {
		*ms = 200;
	}
	else {
		*ms = 500;
	}
	return last_key;
}
int snake()
{
	snake_t* s = create_snake(5, 8, 8, 1);
	egg_t e;
	int key = GAME_KEY_W;
	int time = 100;
	char str[5];
	game_ui_init();
	create_egg(&e, 10);
	pc_step1();//防闪烁第一步
	while (key != 27)
	{
		//key=0;
		clear();//清屏
		get_key(&key);
		s->driction = snake_key(&key, &time);
		//判断加速
		;
		//蛇移动
		snake_go(s);
		if (if_die(s)) {
			clear_snake(s); s = create_snake(5, 8, 8, 1);//死了重新生成蛇
		}
		//画蛇
		draw_snake(s);
		if (if_eat_egg(&e, s) == 0) {
			//draw_circle(e.x, e.y);
			place_egg(&e);
		}
		pc_step2();//防闪烁第二步

		sleep_ms(time);

	}
	//pc_step3();//结束
	//free(s);
	clear_snake(s);
	//while (1);
	return 0;

}

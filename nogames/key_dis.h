#define PC_GAME

#ifdef ESP32_GAME 
#define CUBE_SIZE 8		//每个绘制cbue多少像素
#define CUBE_BETTWEN 1  //间隔
//游戏窗口大小
#define GAME_RANGEX 128/8//8*16
#define GAME_RANGEY 64/8//8*4
#endif
#ifdef PC_GAME 
#define CUBE_SIZE 24		//每个绘制cbue多少像素
#define CUBE_BETTWEN 4  //间隔
//游戏窗口大小
#define GAME_RANGEX 480/CUBE_SIZE//40*16
#define GAME_RANGEY 480/CUBE_SIZE//40*16
#endif
//键值设置
#define GAME_KEY_A 97
#define GAME_KEY_W	119
#define GAME_KEY_D 100
#define GAME_KEY_S 115
//加速键
#define GAME_Q 113


void draw_cube(int x, int y);
void draw_circle(int x, int y);
void draw_dragon(int x, int y);
void draw_box(int x, int y);
void draw_wall_cbue(int x, int y);
void draw_powder_cbue(int x, int y);
void draw_rectangle_wall(int x1, int y1, int x2, int y2);

void game_ui_init();
int get_key(int* ch);

void clear();
void sleep_ms(int ms);

void pc_step1();
void pc_step2();
void pc_step3();

#include "key_dis.h"
#include "stdlib.h"
typedef  unsigned short int  u16;
//俄罗斯方块的缓冲宽度,如果你觉得64位太长可以改成int变32位
typedef  unsigned long long  map_elem;

#ifdef PC_GAME

//真实游戏 高len 宽width,width要小于等于map_elem的位数
#define TRIX_map_LEN	14
#define TRIX_map_WIDTH	8

//绘制偏移量
#define TRIX_MAP_DX 1
#define TRIX_MAP_DY 1

//游戏速度,月小越块
#define GAME_SPEED 30
#endif // PC_GAME

#ifdef ESP32_GAME
#define EXCHANGE_XY

#define TRIX_map_LEN	16
#define TRIX_map_WIDTH	8
#define TRIX_MAP_DX 0
#define TRIX_MAP_DY 0

#endif


//#define EXCHANGE_XY

//4*4俄罗斯方块结构体
//存放方式:
//0	口	4国	 8 口	12口
//1	口	5国	 9 口	13口
//2	口	6国	 10口	14口
//3	口	7国	 11口	15口
//	低字节	  高字节
typedef struct trix_cube_
{
	int x;
	int y;
	u16 sorce;
	char dr;
}trix_cube_t;
//64*pagelen,方块缓存结构体
//存放方式
//同样是竖着的
//宽度是因该小于map_elem的位宽的,
//pagelen则直接是游戏高度
//bit0 bit1 bit2 ... bit(sizeof(map_elem))  pagew
//bit0 bit1 bit2 ... bit(sizeof(map_elem))  ...
//bit0 bit1 bit2 ... bit(sizeof(map_elem))  page1
//bit0 bit1 bit2 ... bit(sizeof(map_elem))  page0
typedef struct trix_mapfer_
{
	int width;
	int pagelen;
	map_elem* sorce;

}trix_map_t;

u16 trix_cube_sorce[7] = {
	0x0232,0x00f0,0x0033,0x0071,0x0170,0x0036,0x0063
};
map_elem mapfer20[20] = { 0 };

//
//数据部分,获取/改变坐标,分配空间
map_elem trix_map_get_xy(trix_map_t map, int x, int y)
{
	int loc = x;
	map_elem u1 = 1;
	if (loc < 0 || loc >= sizeof(map_elem)*8)return 0;
	if (y<0 || y>map.pagelen)return 0;
	map_elem value = u1 << loc;
	value &= map.sorce[y];
	value = value >> loc;
	return value;
}
int trix_map_change_xy(trix_map_t* map, int x, int y, map_elem value)
{
	int loc = x;
	map_elem u1 = 1;
	if (loc < 0 || loc> sizeof(map_elem)*8)return 0;
	//移动到指定位置
	map_elem temp = u1 << loc;
	value = value << loc;
	temp = ~temp;//获取掩码
	map->sorce[y] &= temp;//清除位
	map->sorce[y] |= value;//置位
}

//获取4x4俄罗斯方块的某个值
//x:0-3,y:0-3,返回亮灭
u16 trix_cube_get_xy_4x4(u16 sorce,char x,char y)
{
	int loc = x * 4 + y;
	if (loc < 0 || loc>15)return 0;
	u16 value = 0x0001 << loc;
	value &= sorce;
	value = value >> loc;
	return value;
}
//改变4x4俄罗斯方块的某个值
//x:0-3,y:0-3,value:1或0
int trix_cube_change_xy_4x4(u16* sorce, char x, char y,u16 value)
{
	int loc = x * 4 + y;
	if (loc < 0 || loc>15)return 0;
	//移动到指定位置
	u16 temp = 0x0001 << loc;
	value = value << loc;
	temp = ~temp;//获取掩码
	*sorce &= temp;//清除位
	*sorce |= value;//置位
	return 1;
}
//1(trix_cube)=4*4(cube)
//画一个trix_cube
void draw_a_trix_cube(trix_cube_t cube)
{
	int dx = 0, dy = 0;
	for (dx=0; dx < 4; dx++)
	{
		for (dy=0; dy<4; dy++)
		{
			if (trix_cube_get_xy_4x4((cube.sorce), dx, dy))
#ifdef EXCHANGE_XY
			draw_cube(dy+cube.y+ TRIX_MAP_DY,dx+cube.x+ TRIX_MAP_DX);
#else // CHANGE_XY
			draw_cube(dx + cube.x + TRIX_MAP_DX,dy + cube.y + TRIX_MAP_DY);
#endif
		}
	}
}

void draw_range(trix_map_t map)
{
	int x1, y1, x2, y2;
#ifdef EXCHANGE_XY
	x1 = TRIX_MAP_DY - 1;
	y1 = TRIX_MAP_DX - 1;
	x2 = map.pagelen + TRIX_MAP_DY + 1;
	y2 = map.width + TRIX_MAP_DX + 1;
#else
	x1 = TRIX_MAP_DX - 1;
	y1 = TRIX_MAP_DY - 1;
	x2 = map.width + TRIX_MAP_DX + 1;
	y2 = map.pagelen + TRIX_MAP_DY + 1;
#endif // EXCHANGE_XY	
	draw_rectangle_wall(x1, y1, x2, y2);
}
//绘制地图缓存
void draw_trix_map(trix_map_t map)
{
	int dx = 0, dy = 0;
	for (dx = 0; dx < map.width + 1+ TRIX_MAP_DX; dx++)
	{
	
		for (dy = 0; dy < map.pagelen + 1 + TRIX_MAP_DY; dy++)
		{
			if (trix_map_get_xy(map, dx, dy) && dy != 0)
#ifdef EXCHANGE_XY			
				draw_cube(dy + TRIX_MAP_DY,dx + TRIX_MAP_DX);
#else
				draw_cube(dx + TRIX_MAP_DX, dy + TRIX_MAP_DY);
#endif
		}
	}
}
//逆时针和顺时针旋转俄罗斯方块
//旋转中心在1,1位置
#define CUBE_TURN_RIGHT 0
#define CUBE_TURN_LEFT 1


//为map分配空间
void trix_map_init(trix_map_t* map,int width,int pagelen)
{
	map->pagelen = pagelen;
	map->width = width;
	map->sorce = //mapfer20;
		(map_elem*)malloc(sizeof(map_elem) * (pagelen+4));
	for (int i = 0; i < pagelen+4; i++)
		(map->sorce)[i] = 0;
}
		
void trix_map_delete(trix_map_t* map)
{
	//free(map->sorce);
}

//
//
//逻辑部分

//清除一行,第y行,清除后往下填充
void trix_map_clear_aline(trix_map_t* map,int y)
{
	while (y--)
		map->sorce[y + 1] = map->sorce[y];
	map->sorce[0]=0;
}
//判断是否有一行消除,然后消除它,并让上面的掉下来
void trix_map_flash(trix_map_t* map)
{
	int stat=1;
	int size = sizeof(map_elem)*8-1;
	map_elem full = 0;
	full = ~full;
	full = full << (size - map->width);
	full = full >> (size - map->width);
	for(int y=map->pagelen;y >=0 ;y--)
	{ 
		if (map->sorce[y] >= full)
		{
			trix_map_clear_aline(map, y);
			y = map->pagelen;
		}
	}
} 
void trix_cube_turn(trix_cube_t *cube, char left_right)
{
	u16 value = 0;
	u16 derst = 0;
	for (int x = 0; x < 3; x++)
	{
		for (int y = 0; y < 3; y++)
		{
			value = trix_cube_get_xy_4x4(cube->sorce, x, y);

			if (left_right == CUBE_TURN_LEFT)
				trix_cube_change_xy_4x4(&derst, 2 - y, x, value);
			if (left_right == CUBE_TURN_RIGHT)
				trix_cube_change_xy_4x4(&derst, y, 2 - x, value);
		}
	}
	for (int i = 0; i < 4; i++)
	{
		value = trix_cube_get_xy_4x4(cube->sorce, i, 3);
		trix_cube_change_xy_4x4(&derst, 3, i, value);
		value = trix_cube_get_xy_4x4(cube->sorce, 3, i);
		trix_cube_change_xy_4x4(&derst, i, 3, value);
	}
	cube->sorce = derst;
}
//获取随机的俄罗斯方块
trix_cube_t get_rand_trix_cube()
{
	int r = rand();
	trix_cube_t c1;
	c1.x = 2; c1.y = -4;
	c1.sorce = trix_cube_sorce[r % 7];
	for (int i = 0; i < (r % 4); i++)
		trix_cube_turn(&c1, CUBE_TURN_RIGHT);
	return c1;
}


//将俄罗斯方块放在地图缓存上
void palce_trix_cube_in_map(trix_cube_t* cube,trix_map_t* map)
{
	u16 value;
	for (int x=0;x<4;x++)
	{
		for (int y = 0; y < 4; y++)
		{
			value=trix_cube_get_xy_4x4((cube->sorce),x,y);
			if (value)//只添加为1的区块
			trix_map_change_xy(map, cube->x + x, cube->y + y, 1);				
		}
	}
}

//第一bit返回是否到触底,能改变x,y则改变,否则不变
//第二bit返回是否碰撞到maper
int if_can_mov_xy(trix_map_t* map,trix_cube_t* cube, int per_cubex,int per_cubey)
{
	static int x_last,y_last;
	static int tick;
	int width = map->width, high = map->pagelen;
	int stat = 0;
	if (per_cubex < 0)(cube->x) ++;
	if (per_cubex >width)(cube->x) --;
	if (trix_map_get_xy(*map, per_cubex, per_cubey))
	{
		cube->x = x_last; stat |= 2;
	}
	if (per_cubey > high){
		cube->y --; stat |= 1;
	}
	if (cube->y != y_last){
		if (trix_map_get_xy(*map, per_cubex, per_cubey)) {
				stat |= 3; //stat |= 2;
				cube->y--;
		}				
	}
	tick++;
	if (tick == 16)
	{
		x_last = cube->x;
		y_last = cube->y;
		tick = 0;
	}
	return stat;
}



//判断俄罗斯方块是否触底
//俄罗斯方块对缓冲地图的碰撞逻辑
int if_trix_cube_floor(trix_map_t* map,trix_cube_t* cube)
{
	int value;
	int stat=0;
	for (int y = 3; y >= 0; y--){
		for (int x = 0; x < 4; x++){
			value = trix_cube_get_xy_4x4((cube->sorce), x, y);
			if (value) {//检测到俄罗斯方块的一小块

				stat |= (0x01 & if_can_mov_xy(map,cube, (cube->x)+x, (cube->y)+y));	
				//if (stat == 1)
				//	stat++;
			}
		}
	}
	return stat;
}

void if_can_turn(trix_map_t* map, trix_cube_t* cube)
{
	int value;
	int stat = 0;
	trix_cube_turn(cube,1);
	for (int y = 3; y >= 0; y--) {
		for (int x = 0; x < 4; x++) {
			value = trix_cube_get_xy_4x4((cube->sorce), x, y);
			if (value) {//检测到俄罗斯方块的一小块

				stat |= 0x02 & if_can_mov_xy(map, cube, (cube->x) + x, (cube->y) + y);
			}
		}
	}
	if(stat)trix_cube_turn(cube, 0);
}
int if_trix_die(trix_map_t* map)
{
	int x = map->width;
	int y = map->pagelen;
	int tick = 1;
	while (x--)
	{
		if (trix_map_get_xy(*map, x, 0))
			return 1;
	}
	return 0;
}
//测试显示
void cube_test()
{
	trix_cube_t c1;
	c1.x = 1;
	c1.y = 1;
	for (int j = 0; j < 7; j++)
	{
		c1.sorce = trix_cube_sorce[j];
		for (int i = 0; i < 4; i++)
		{
			clear();
			trix_cube_turn(&c1,0);
			draw_a_trix_cube(c1);
			sleep_ms(200);
			pc_step2();
		}
	}
	for (int j = 0; j < 20; j++)
	{
		clear();
		c1 = get_rand_trix_cube();	
		draw_a_trix_cube(c1);
		sleep_ms(500);
		pc_step2();
	}
}
void trix_key(int key, trix_map_t* map,trix_cube_t *cube)
{
	switch (key)
	{
		case GAME_KEY_A: (cube->x)--; break;
		case GAME_KEY_D: (cube->x)++; break;
		case GAME_KEY_S: (cube->y)++; break;
		case GAME_KEY_W: if_can_turn(map,cube); break;
	};
}

void trix()
{
	int key, x=0, y=0,stat=0;
	int game_over=0;
	trix_cube_t c1;
	trix_map_t map;
	int temp;
	trix_map_init(&map,TRIX_map_WIDTH ,TRIX_map_LEN);
	int tick=0;
	while(!game_over)
	{ 
		c1=get_rand_trix_cube();
		stat = 0;
		while (!stat)
		{

			clear();
			key = 0;
			get_key(&key);
			trix_key(key,&map,&c1);
			stat=if_trix_cube_floor(&map, &c1);	
			draw_a_trix_cube(c1);
			draw_trix_map(map);
			draw_range(map);
			trix_map_flash(&map);
			if(tick==GAME_SPEED) {
				c1.y++;
				tick = 0;
			}
			tick++;
			pc_step2();
			sleep_ms(10);
			if (key == 27)stat = 1;
			
		}
		palce_trix_cube_in_map(&c1, &map);
		game_over = if_trix_die(&map);
		//pc_step2();
		if (key == 27)game_over = 1;
	}
	trix_map_delete(&map);

}
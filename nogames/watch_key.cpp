


#include <iostream>
#include <cstdlib>
//#include "SerialInterface.h"
#include <windows.h>
#include <tchar.h>
#include "key_dis.h"
#include <cstringt.h>
#include <stdio.h>
//#include "serial.h"
#include <windows.h>

//#include <TCHAR.H>   

using namespace std;

HANDLE hCom; //全局变量，串口句柄

int serial_open(LPCWSTR COMx, int BaudRate) {

	hCom = CreateFile(COMx, //COM1口    
		GENERIC_READ | GENERIC_WRITE, //允许读和写    
		0, //独占方式    
		NULL,
		OPEN_EXISTING, //打开而不是创建     
		0, //重叠方式FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED  (同步方式设置为0)
		NULL);
	if (hCom == INVALID_HANDLE_VALUE)
	{
		printf("打开COM失败!\n");
		return FALSE;
	}
	SetupComm(hCom, 1024, 1024); //输入缓冲区和输出缓冲区的大小都是1024 

					 //设定读写超时
							/*COMMTIMEOUTS TimeOuts;
				 TimeOuts.ReadIntervalTimeout=1000;
				 TimeOuts.ReadTotalTimeoutMultiplier=500;
				 TimeOuts.ReadTotalTimeoutConstant=5000; //设定写超时
				 TimeOuts.WriteTotalTimeoutMultiplier=500;
				 TimeOuts.WriteTotalTimeoutConstant = 2000;
				 SetCommTimeouts(hCom, &TimeOuts); //设置超时
				 */
	DCB dcb;
	GetCommState(hCom, &dcb);
	dcb.BaudRate = BaudRate;		//设置波特率为BaudRate
	dcb.ByteSize = 8;					//每个字节有8位 
	dcb.Parity = NOPARITY;			//无奇偶校验位 
	dcb.StopBits = ONESTOPBIT;		//一个停止位
	SetCommState(hCom, &dcb);		//设置参数到hCom
	PurgeComm(hCom, PURGE_TXCLEAR | PURGE_RXCLEAR);//清空缓存区		//PURGE_TXABORT 中断所有写操作并立即返回，即使写操作还没有完成。
												   //PURGE_RXABORT 中断所有读操作并立即返回，即使读操作还没有完成。
												   //PURGE_TXCLEAR 清除输出缓冲区 
												   //PURGE_RXCLEAR 清除输入缓冲区  
	return TRUE;
}
int serial_write(char lpOutBuffer[])	//同步写串口
{
	DWORD dwBytesWrite = sizeof(lpOutBuffer);
	COMSTAT ComStat;
	DWORD dwErrorFlags;
	BOOL bWriteStat;
	ClearCommError(hCom, &dwErrorFlags, &ComStat);
	bWriteStat = WriteFile(hCom, lpOutBuffer, dwBytesWrite, &dwBytesWrite, NULL);
	if (!bWriteStat)
	{
		printf("写串口失败!\n");
		return FALSE;
	}
	PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
	return TRUE;
}
void Serial_close(void)		//关闭串口
{
	CloseHandle(hCom);
}


int watch_key()
{
	int key = 0; 
	int stat;
	char send_buf[5] = { 0 };
	stat=serial_open(_T("COM5"), 115200); //打开COM5，波特率为115200
	if(!stat)cout << "open err" << endl;
	else cout << "open com5" << endl;
	while(1)
	{ 
		if (get_key(&key))
		{
			sprintf_s(send_buf, "#%s\n\r", (char*)&key);
			stat = serial_write(send_buf);
			if(!stat)cout << "send err" << endl;
			else cout << "send:" << send_buf << endl;
		}
	}
	Serial_close();//关闭串口
	system("pause");
	return 0;

}

//all can include
#include "stdlib.h"
#include "key_dis.h"

//windows include
#ifdef PC_GAME
#include <graphics.h>		// 引用 EasyX 绘图库头文件
#include <io.h>
#include <conio.h>
#include <iostream>
#endif

//ESP32 include
#ifdef ESP32_GAME
//extern "C"
#include "freertos/FreeRTOS.h"
#include "pic_al.h"
#include "oled.h"
#include "stdio.h"
#endif



//
// 以下都是你需要修改的函数
#ifdef PC_GAME
//绘制游戏窗口,单片机可以改成初始化屏幕
void game_ui_init()
{
	initgraph(GAME_RANGEX * CUBE_SIZE, GAME_RANGEY * CUBE_SIZE);
}
//在x,y位置画矩形，长宽为width
//不是算40一格一像素,而是你设备的真实像素
void draw_easyx(int x, int y, int width)
{
	fillrectangle(x, y, x + width, y + width);
}
//绘制失心填充,按照你define的游戏方块大小来
void draw_cube(int x, int y)
{
	//fillrectangle(x * CUBE_SIZE, y * CUBE_SIZE, x * CUBE_SIZE + CUBE_SIZE - CUBE_BETTWEN, y * CUBE_SIZE + CUBE_SIZE - CUBE_BETTWEN);
	draw_box(x, y);
}
//画失心园,作为奖励,也可以换更改成draw_cube,更像素风
void draw_circle(int x, int y)
{
	fillcircle(x * CUBE_SIZE + CUBE_SIZE / 2, y * CUBE_SIZE + CUBE_SIZE / 2, CUBE_SIZE / 2 - CUBE_BETTWEN);
	//draw_cube(x,y);
}
void clear()
{
	cleardevice();//清屏
}
void sleep_ms(int ms)
{
	Sleep(ms);
}
//获取蛇的按键值ch：获取到的键值，
//返回值：蛇的方向
int get_key(int* ch)
{
	static int key = GAME_KEY_W;
	if (_kbhit()) {//如果有按键按下，则_kbhit()函数返回真
		*ch = _getch();//使用_getch()函数获取按下的键值
		return 1;
	}
	return 0;
}

//防闪烁第一步,单片机可以清除缓存
void pc_step1()
{
	BeginBatchDraw();
}
//防闪烁第二步,单片机可以直接将缓存画在屏幕上
void pc_step2()
{
	FlushBatchDraw();
}
void pc_step3()
{
	EndBatchDraw();//结束
}
#endif



#ifdef ESP32_GAME
//绘制游戏窗口,单片机可以改成初始化屏幕
void game_ui_init()
{
	OLED_Init();
}
//直接画一格像素了
void draw_easyx(int x, int y, int width)
{
	put_xy(x, y);
}
//绘制失心填充,按照你define的游戏方块大小来
void draw_cube(int x, int y)
{
	int i = 0, j = 0;
	for (; i < CUBE_SIZE - CUBE_BETTWEN; i++)
		for (j = 0; j < CUBE_SIZE - CUBE_BETTWEN; j++)
			put_xy(x * 8 + i, y * 8 + j);
}
//画失心园,作为奖励,也可以换更改成draw_cube,更像素风
void draw_circle(int x, int y)
{
	//fillcircle(x * CUBE_SIZE + CUBE_SIZE / 2, y * CUBE_SIZE + CUBE_SIZE / 2, CUBE_SIZE / 2 - CUBE_BETTWEN);
	draw_cube(x, y);
}
void clear()
{
	clear_buf();//清屏
}
void sleep_ms(int ms)
{
	vTaskDelay(ms);
}
//获取蛇的按键值ch：获取到的键值，
//返回值：是否按键
int get_key(int* ch)
{
	if (serial_key(ch))return 1;
	else return 0;
	//return *ch;
}

//防闪烁第一步,单片机可以清除缓存
void pc_step1()
{
	//BeginBatchDraw();
}
//防闪烁第二步,单片机可以直接将缓存画在屏幕上
void pc_step2()
{
	draw_buf();
}
void pc_step3()
{
	//	EndBatchDraw();//结束
}



#endif




//
//
//以下是你可能不需要修改的
#define PICTURE_PER8x8 CUBE_SIZE/8//恐龙每格绘画方块实际像素大小
//电脑默认是40/8,5格像素合成一格

//在一个cube内绘制8x8的黑白像素图片,使用这个函数可以绘制各种图标,
//当然如果你没有多彩多样的图标,直接忽略掉里面写draw_cube()就行了
void draw_cube_8x8(int x, int y, const char* picture)
{
	int x1 = x * CUBE_SIZE;
	int y1 = y * CUBE_SIZE;
	int i = 0, j = 0;
	for (; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			if (picture[i] & (0x01 << j))
				draw_easyx(x1 + i * PICTURE_PER8x8, y1 + j * PICTURE_PER8x8, PICTURE_PER8x8);
		}
	}
}


// 
//画小恐龙,CUBE_SIZE(40)格像素大小,实际设计为8格方块,每格5像素
//若设备想更改其他的,请自行加图片绘制
//画小恐龙
static const char picture_dragon[8] =
{ 0x20,0x30,0x7E,0x3F,0x7D,0x17,0x06,0x00 };
void draw_dragon(int x, int y)
{
	draw_cube_8x8(x, y, picture_dragon);
}
//画箱子
static const char picture_box[8] =
{ 0x7F,0x79,0x49,0x67,0x67,0x49,0x79,0x00 };
void draw_box(int x, int y)
{
	draw_cube_8x8(x, y, picture_box);
}
//画墙
static const char picture_wall[8] =
{ 0x7F,0x41,0x45,0x49,0x51,0x41,0x7F,0x00 };
void draw_wall_cbue(int x, int y)
{
	draw_cube_8x8(x, y, picture_wall);
}
//画火药
static const char picture_powder[8] =
{ 0x18,0x34,0x2A,0x35,0x2A,0x34,0x18,0x00 };
void draw_powder_cbue(int x, int y)
{
	draw_cube_8x8(x, y, picture_powder);
}
//绘制直线,0向右1向下
void draw_cbue_line(int x, int y,int len,int dr)
{
	while (len--) {
		if(dr==0)
			draw_cube_8x8(x+len, y, picture_wall);
		else
			draw_cube_8x8(x ,y+len, picture_wall);
	}	
}
//绘制直线,0向右1向下
void draw_rectangle_wall(int x1, int y1, int x2, int y2)
{
	draw_cbue_line(x1, y1, x2 - x1, 0);
	draw_cbue_line(x1, y1, y2 - y1, 1);
	draw_cbue_line(x1, y2, x2 - x1, 0);
	draw_cbue_line(x2, y1, y2 - y1+1, 1);
}
//各种必要的库
//bool all_buf_get_xy(void* type_buf, int x, int y, size_t type_size)
//{
//	int loc = x * type_size + y;
//	if (loc < 0 || loc>type_size)return 0;
//	unsigned long long value = 1 << loc;
//	value &= type_buf[x];
//	value = value >> loc;
//
//
//}

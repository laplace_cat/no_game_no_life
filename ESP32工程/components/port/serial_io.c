/*
 * serial_io.c
 *
 *  Created on: 2022��6��16��
 *      Author: 17103
 */


#include "driver/uart.h"
#include "stdio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "port.h"

static xQueueHandle key_q = NULL;

void key_q_init()
{
	key_q = xQueueCreate(10, sizeof(int));
}


void serial_key_task()
{

	int key[2]={0};
	char key_buf[5]={0};
	//char k=-1;
	//uart_read_bytes(0,key_buf,2,1);
	while(1)
	{
		fgets(key_buf,5,stdin);
		if(key_buf[0]=='#')
		{
			//printf("key:%s\n",key_buf+1);
			key[0]=key_buf[1];
			//xQueueSend(key_q, &key, NULL);
			if(key_q!=NULL)
			{
				if(xQueueSend(key_q,key,1));
			}
			key_buf[1]=0;
			key_buf[0]=0;

		}
		delay_ms(1);
	}


}

void any_key_send(char *buf,int len)
{
	int i=0;
	int key=0;
	for(;i<len-1;i++)
	{
		if(buf[i]=='#')
		{
			//printf("key:%s\n",key_buf+1);
			key=buf[i+1];
			//xQueueSend(key_q, &key, NULL);
			if(key_q!=NULL)
			{
				if(xQueueSend(key_q,&key,1));
			}
			//key_buf[1]=0;
			key=0;
		}
	}
}



int serial_key(int* key)
{
	if(xQueueReceive(key_q, key, 1))
	{
		printf("key:%d\n",*key);
		vTaskDelay(10);
		xQueueReset(key_q);
	}

	return 0;
}

idf_component_register(SRCS "8080.c"
						SRCS "spi.c"
						SRCS "soft_spi.c"
						SRCS "esp32timer.c"
						SRCS "serial_io.c"
						SRCS "my_wifi.c"
                    INCLUDE_DIRS "include"
                     PRIV_REQUIRES nvs_flash
                    )


/*
 * esp32timer.c
 *
 *  Created on: 2022年6月13日
 *      Author: 17103
 */

#include "driver/timer.h"
#define TIMER_DIVIDER         (8000)  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds

void eap32_timer00_init()
{
    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_DIS,
        .auto_reload = TIMER_AUTORELOAD_EN,//自动重计数
    }; // default clock source is APB
    timer_init(0, 0, &config);//配置0组0号定时器
    timer_start(0,0);
	 //timer_get_alarm_value();
}
void p_timer()
{
	uint64_t t;
	timer_get_counter_value(0,0,&t);
	//if(t%5000==0)
	{
		printf("t:%lld\n",t);
	}

}
uint64_t get_timer0()
{
	uint64_t t;
	timer_get_counter_value(0,0,&t);
	return t;
}
void clear_timer0()
{
	timer_set_counter_value(0,0,0);
}

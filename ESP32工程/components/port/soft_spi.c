/*
 * soft_spi.c
 *
 *  Created on: 2022年6月5日
 *      Author: 17103
 */

#include "soft_spi.h"
#include "port.h"
#ifdef SOFT_SPI
void OLED_WR_Byte(u8 dat,u8 cmd)
{
	u8 i;
	if(cmd)
	{
	  OLED_DC_Set();
	}
	else
	  OLED_DC_Clr();
	OLED_CS_Clr();
	for(i=0;i<8;i++)
	{
		OLED_SCLK_Clr();
		if(dat&0x80)
		{
		   OLED_SDIN_Set();
		}
		else
		   OLED_SDIN_Clr();
		OLED_SCLK_Set();
		dat<<=1;
	}
	OLED_CS_Set();
	OLED_DC_Set();
}

void oled_send_cmd(u8 cmd)
{
	OLED_WR_Byte(cmd,OLED_CMD);
}
void oled_send_adata(u8 data)
{
	OLED_WR_Byte(data,OLED_DATA);
}
void oled_send_datas(u8* data,int len)
{
	int i=0;
	for(;i<len;i++)
	{
		oled_send_adata	(data[i]);
	}
}
void oled_gpio_init()
{
	gpio_config_t  oled;
	oled.intr_type = GPIO_INTR_DISABLE;
	oled.pin_bit_mask=OLED_PIN;
	oled.mode=GPIO_MODE_OUTPUT;
	oled.pull_down_en=1;
	oled.pull_up_en=0;
	gpio_config(&oled);

}
#endif



//在指定位置显示一个字符,包括部分字符
//x:0~127
//y:0~63
//mode:0,反白显示;1,正常显示
//size:选择字体 16/12
//void OLED_ShowChar(u8 x,u8 y,u8 chr)
//{
//	unsigned char c=0,i=0;
//		c=chr-' ';//得到偏移后的值
//		if(x>Max_Column-1){x=0;y=y+2;}
//		if(SIZE ==16)
//			{
//			OLED_Set_Pos(x,y);
//			for(i=0;i<8;i++)
//			OLED_WR_Byte(F8X16[c*16+i],OLED_DATA);
//			OLED_Set_Pos(x,y+1);
//			for(i=0;i<8;i++)
//			OLED_WR_Byte(F8X16[c*16+i+8],OLED_DATA);
//			}
//			else {
//				OLED_Set_Pos(x,y+1);
//				for(i=0;i<6;i++)
//				OLED_WR_Byte(F6x8[c][i],OLED_DATA);
//
//			}
//}
//m^n函数

//显示2个数字
//x,y :起点坐标
//len :数字的位数
//size:字体大小
//mode:模式	0,填充模式;1,叠加模式
//num:数值(0~4294967295);
//void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size)
//{
//	u8 t,temp;
//	u8 enshow=0;
//	for(t=0;t<len;t++)
//	{
//		temp=(num/oled_pow(10,len-t-1))%10;
//		if(enshow==0&&t<(len-1))
//		{
//			if(temp==0)
//			{
//				OLED_ShowChar(x+(size/2)*t,y,' ');
//				continue;
//			}else enshow=1;
//
//		}
//	 	OLED_ShowChar(x+(size/2)*t,y,temp+'0');
//	}
//}

////显示一个字符号串
//void OLED_ShowString(u8 x,u8 y,u8 *chr)
//{
//	unsigned char j=0;
//	while (chr[j]!='\0')
//	{		OLED_ShowChar(x,y,chr[j]);
//			x+=8;
//		if(x>120){x=0;y+=2;}
//			j++;
//	}
//}
//显示汉字
//void OLED_ShowCHinese(u8 x,u8 y,u8 no)
//{
//	u8 t,adder=0;
//	OLED_Set_Pos(x,y);
//    for(t=0;t<16;t++)
//		{
//				OLED_WR_Byte(Hzk[2*no][t],OLED_DATA);
//				adder+=1;
//     }
//		OLED_Set_Pos(x,y+1);
//    for(t=0;t<16;t++)
//			{
//				OLED_WR_Byte(Hzk[2*no+1][t],OLED_DATA);
//				adder+=1;
//      }
//}

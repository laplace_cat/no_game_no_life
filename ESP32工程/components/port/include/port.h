/*
 * port.h
 *
 *  Created on: 2022��6��10��
 *      Author: 17103
 */
#ifndef COMPONENTS_PORT_INCLUDE_PORT_H_
#define COMPONENTS_PORT_INCLUDE_PORT_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "stdlib.h"
#define SOFT_SPI

typedef uint8_t u8;
typedef uint32_t u32;

#define delay_ms(t) vTaskDelay(t)
#define sleep_us() usleep()

//SOFT_SPICONF
#define DIN 19
#define SCLK 18
#define CS 5
#define RES 17
#define DC 16

#define KEY1 1
#define KEY2 2
#define KEY3 3
#define KEY4 4


void oled_send_cmd(u8 cmd);
void oled_send_adata(u8 data);
void oled_send_datas(u8* data,int len);
void oled_gpio_init();

void p_timer();
void eap32_timer00_init();
uint64_t get_timer0();
void clear_timer0();
int serial_key(int* key);
void serial_key_task();
void io_init_io();

void any_key_send(char *buf,int len);
void key_q_init();
void tcp_client_task();
void wifi_init_sta(void);
void wifi_connect();
QueueHandle_t gpio_evt_queue;

#endif /* COMPONENTS_PORT_INCLUDE_PORT_H_ */

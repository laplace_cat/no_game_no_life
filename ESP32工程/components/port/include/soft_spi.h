/*
 * soft_spi.h
 *
 *  Created on: 2022年6月5日
 *      Author: 17103
 */

#ifndef COMPONENTS_PORT_INCLUDE_SOFT_SPI_H_
#define COMPONENTS_PORT_INCLUDE_SOFT_SPI_H_

#include "driver/gpio.h"
#include "stdlib.h"
#include "port.h"
//OLED模式设置
//0:4线串行模式
//1:并行8080模式

#define OLED_MODE 0
#define SIZE 16
#define XLevelL		0x00
#define XLevelH		0x10
#define Max_Column	128
#define Max_Row		64
#define	Brightness	0xFF
#define X_WIDTH 	128
#define Y_WIDTH 	64

#define OLED_PIN (1ULL<<DIN)|(1ULL<<SCLK)|(1ULL<<CS)|(1ULL<<RES)|(1ULL<<DC)

//-----------------OLED端口定义----------------
#define OLED_SCLK_Clr() gpio_set_level(SCLK,0);
#define OLED_SCLK_Set() gpio_set_level(SCLK,1);

#define OLED_SDIN_Clr() gpio_set_level(DIN,0);
#define OLED_SDIN_Set() gpio_set_level(DIN,1);

#define OLED_RST_Clr() gpio_set_level(RES,0);
#define OLED_RST_Set() gpio_set_level(RES,1);

#define OLED_DC_Clr() gpio_set_level(DC,0);
#define OLED_DC_Set() gpio_set_level(DC,1);

#define OLED_CS_Clr()  gpio_set_level(CS,0);
#define OLED_CS_Set()  gpio_set_level(CS,1);

//#define delay_ms(t) vTaskDelay(t);

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据


//OLED控制用函数
void OLED_WR_Byte(u8 dat,u8 cmd);


#endif /* COMPONENTS_PORT_INCLUDE_SOFT_SPI_H_ */


#include "key_dis.h"


//一个page有8个墙cube,竖着排列,一个page大小为一个char,1个字节
// page0	page1
// 口		口
// 口		口
// 口		...
// 口		...
// 口
// 口
// 口
// 口
// page数为9,high为1,代表地图总共有9格宽*8格高大小
// 如果page为8,high为2,代表地图是4格宽*16格高
//8(cube)* high 格高的地图,使用map数组绘制
typedef struct wall_ {
	char* map;
	int high;
	int page_len;
	void (*draw_func)(int x, int y);
	//	int x_size(wall_t);
}wall_t;

#define MAP1_PAGE_LEN 9
#define MAP1_PAGE_HIGH 1

#define MAP2_PAGE_LEN 8
#define MAP2_PAGE_HIGH 1

//存放地图墙面信息
char wall_xy[9] = {
	0xFF,0x81,0x85,0x91,0x87,0xD2,0x42,0x72,0x1E
};
//存放箱子信息
char box_xy[9] = {
	0x00,0x00,0x08,0x20,0x10,0x00,0x18,0x00
};
//存放箱子备份,方便重开
const char box_xy_per[9] = {
	0x00,0x00,0x08,0x20,0x10,0x00,0x18,0x00
};
//存放箱子归为信息
char box_suit_xy[9] = {
	0x00,0x70,0x50,0x00,0x00,0x00,0x00,0x00,0x00
};



//存放地图墙面信息
char wall2_xy[8] = {
	0xF0,0x9C,0x87,0xA1,0x81,0x87,0x9C,0xF0
};
//存放箱子信息
char box2_xy[8] = {
	0x00,0x00,0x00,0x10,0x28,0x20,0x00,0x00
};
//存放箱子备份,方便重开
const char box2_xy_per[8] = {
	0x00,0x00,0x00,0x10,0x28,0x20,0x00,0x00
};
//存放箱子归为信息
char box2_suit_xy[8] = {
	0x00,0x00,0x00,0x02,0x06,0x08,0x00,0x00
};

//存放地图墙面信息
char wall3_xy[8] = {
	0x7F,0x41,0x41,0x63,0x41,0x41,0x49,0x7F
};
//存放箱子信息
char box3_xy[8] = {
	0x00,0x00,0x1C,0x00,0x08,0x14,0x00,0x00
};
//存放箱子备份,方便重开
const char box3_xy_per[8] = {
	0x00,0x00,0x1C,0x00,0x08,0x14,0x00,0x00
};
//存放箱子归为信息
char box3_suit_xy[8] = {
	0x00,0x00,0x00,0x0C,0x1C,0x00,0x00,0x00
};

//返回地图宽度
int x_size(wall_t w)
{
	return w.page_len / w.high;
}

//返回,x,y处是否有东西,有是1,没有是0
//wall存放墙面位置的char数组
//high 一个high有8格
//len wall数组的总长度
char get_wall_value_xy(wall_t w,int x,int y)
{
	char value=0;
	int in_page = x + y / 8 * w.page_len / w.high;
	char in_a_page = y % 8;
	if (w.map[in_page] & 0x01 << in_a_page)
		value = 1;
	return value;
}
void change_wall_value_xy(wall_t w, int x, int y,char value)
{
	int in_page = x + y / 8 * w.page_len / w.high;
	char in_a_page = y % 8;	
	int temp;
	temp = (0x01 << in_a_page);
	if (value)
		w.map[in_page] |= temp;
	else
		w.map[in_page] &= ~temp;
}

//绘制墙
void draw_wall_hx8(wall_t w)
{
//	void* p;
	int x = 0, y = 0;
	for (; x < w.page_len/w.high; x++)
	{
		for (y = 0; y < w.high*8; y++)
		{
			if(get_wall_value_xy(w,x,y))
				w.draw_func(x,y);
				
		}
	}
}
wall_t wall_init(char *map,int high,int page_len,void (* draw_func)(int x,int y))
{
	wall_t wall;
	wall.high = 1;
	wall.page_len = 9;
	wall.map = wall_xy;
	wall.draw_func = draw_func;
	return wall;
}
//分别输入墙和箱子位置还有page_len,和page_highd的信息
void map1_init(wall_t *wall, wall_t* boxs, wall_t* powder,
	char* map_wall,char* map_box,char* map_powder,
	int page_high,int page_len)
{
	wall->high = page_high;
	wall->page_len = page_len;
	wall->map = map_wall;
	wall->draw_func = draw_wall_cbue;
	*boxs = *wall;
	boxs->map = map_box;
	boxs->draw_func = draw_box;
	*powder = *wall;
	powder->map = map_powder;
	powder->draw_func = draw_powder_cbue;
}

//判断箱子和墙和小恐龙的逻辑,碰撞逻辑
//返回值不重要,主要在这里面逻辑已经写好了
//可以用返回值刷新屏幕
int if_dragon_can_move(wall_t wall,wall_t boxs,int* x,int* y)
{
	static int x_last=1, y_last=1;
	char value=1;
	if (*x == x_last && *y == y_last)return 1;

	//若是前面有墙则走不动
	value=get_wall_value_xy(wall, *x, *y);
	if (value)
	{
		*x = x_last;
		*y = y_last;
		return 0;
	}
	value= get_wall_value_xy(boxs, *x, *y);
	if(value)
	{
		//前面没有其他墙或箱子才能推动
		value = get_wall_value_xy(wall, (*x) * 2 - x_last, (*y) * 2 - y_last);
		value |= get_wall_value_xy(boxs, (*x) * 2 - x_last, (*y) * 2 - y_last);
		if(value)
		{
			*x = x_last;
			*y = y_last;
			return 0;
		}
		//终于是能推动的情况了
		change_wall_value_xy(boxs, *x , *y , 0);//清除原来位置
		change_wall_value_xy(boxs, (* x ) * 2 - x_last, (*y) * 2 - y_last, 1);//放在新位置
	}
	x_last = *x;
	y_last = *y;
	return 1;
}
//苦力怕和火药一一对应就赢了
int if_win(wall_t boxs, wall_t powder)
{
	int value;
	int stat;
	int i,j;
	for(i=0;i< boxs.page_len/boxs.high;i++)
		for (j = 0; j < 8 * boxs.high; j++)
		{
			value= get_wall_value_xy(boxs, i, j);
			if (value != get_wall_value_xy(powder, i, j))
				return 0;
		}
	return 1;
}

void dragon_move(int* x,int* y,int driction)
{
	switch (driction)
	{
		case GAME_KEY_W: (* y)--; break;
		case GAME_KEY_S: (*y)++; break;
		case GAME_KEY_A: (*x)--; break;
		case GAME_KEY_D: (*x)++; break;
	}
}
//若要修改或添加地图,只需修改map1_init()中内容
//返回游戏是否结束
int one_map_game(wall_t wall, wall_t boxs,wall_t powder ,const char* box_per,int* x, int* y)
{
	clear();
	int key = 0;
	int stat = 0;
	get_key(&key);
	//移动
	dragon_move(x, y, key);
	if_dragon_can_move(wall, boxs, x, y);
	//绘制
	draw_dragon(*x, *y);
	draw_wall_hx8(wall);
	draw_wall_hx8(boxs);
	draw_wall_hx8(powder);
	//刷新屏幕
	pc_step2();
	//sleep_ms(500);
	//判断胜利
	stat = if_win(boxs, powder);
	if (key == GAME_Q)
	{
		for (int i = 0; i < boxs.page_len; i++)
			boxs.map[i] = box_per[i];
	}
	else if (key == 27)stat = 1;
	//else stat = 0;
	return stat;
}

void box()
{
	wall_t w1,boxs1,powder1;
	//初始化地图
	//game_ui_init();
	int x = 1, y = 1;//小恐龙初始坐标
	int key = 0;
	int stat=0;
	pc_step1();

	x = 2; y = 5;
	map1_init(&w1, &boxs1, &powder1,
		wall2_xy, box2_xy, box2_suit_xy,
		MAP2_PAGE_HIGH, MAP2_PAGE_LEN);
	while (!stat)
	{
		stat = one_map_game(w1, boxs1, powder1, box2_xy_per, &x, &y);
	}
	x = 1; y = 1;
	map1_init(&w1, &boxs1, &powder1,
		wall_xy, box_xy, box_suit_xy,
		MAP1_PAGE_HIGH, MAP1_PAGE_LEN);
	stat = 0;
	while (!stat)
	{
		stat = one_map_game(w1, boxs1, powder1, box_xy_per, &x, &y);
	}
	x = 1; y = 1;
	map1_init(&w1, &boxs1, &powder1,
		wall3_xy, box3_xy, box3_suit_xy,
		MAP1_PAGE_HIGH, 8);
	stat = 0;
	while (!stat)
	{
		stat = one_map_game(w1, boxs1, powder1, box_xy_per, &x, &y);
	}
	pc_step3();
}

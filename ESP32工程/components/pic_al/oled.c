/*
 * oled.c
 *
 *  Created on: 2021年11月16日
 *      Author: 17103
 */
#include <stdio.h>
#include "soft_spi.h"
#define BEGIN_XY(x0,y0) ((y0/8)*128+x0)
#define END_XY(x1,y1) ((y1/8)*128+x1)


void OLED_Set_Pos(unsigned char x, unsigned char y)
{
	oled_send_cmd(0xb0+y);
	oled_send_cmd((((x+2)&0xf0)>>4)|0x10);
	oled_send_cmd(((x+2)&0x0f));
}
//开启OLED显示
void OLED_Display_On(void)
{
	oled_send_cmd(0X8D);  //SET DCDC命令
	oled_send_cmd(0X14);  //DCDC ON
	oled_send_cmd(0XAF);  //DISPLAY ON
}
//关闭OLED显示
void OLED_Display_Off(void)
{
	oled_send_cmd(0X8D);  //SET DCDC命令
	oled_send_cmd(0X10);  //DCDC OFF
	oled_send_cmd(0XAE);  //DISPLAY OFF
}
//清屏函数,清完屏,整个屏幕是黑色的!和没点亮一样!!!
void OLED_Clear(void)
{
	u8 i,n;
	for(i=0;i<8;i++)
	{
		oled_send_cmd (0xb0+i);    //设置页地址（0~7）
		oled_send_cmd (0x02);      //设置显示位置—列低地址
		oled_send_cmd (0x10);      //设置显示位置—列高地址
		for(n=0;n<128;n++)oled_send_adata(0);
	} //更新显示
}

/***********功能描述：显示显示BMP图片128×64起始点坐标(x,y),x的范围0～127，y为页的范围0～7*****************/
void OLED_DrawBMP(unsigned char x0, unsigned char y0,unsigned char x1, unsigned char y1,unsigned char* BMP)
{
 unsigned char y;
  if(y1%8==0) y=y1/8;
  else y=y1/8+1;
	for(y=y0;y<y1;y++)
	{
		OLED_Set_Pos(x0,y);
		oled_send_datas(BMP+(y-y0)*(x1-x0),x1-x0);
//    for(x=x0;x<x1;x++)
//	    {
//    		oled_send_adata(BMP[j++],OLED_DATA);
//	    }
	}
}

//初始化SSD1306
void OLED_Init(void)
{
	OLED_RST_Set();
	delay_ms(100);
	OLED_RST_Clr();
	delay_ms(200);
	OLED_RST_Set();

	OLED_WR_Byte(0xAE,OLED_CMD);//--turn off oled panel
	OLED_WR_Byte(0x02,OLED_CMD);//---set low column address
	OLED_WR_Byte(0x10,OLED_CMD);//---set high column address
	OLED_WR_Byte(0x40,OLED_CMD);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
	OLED_WR_Byte(0x81,OLED_CMD);//--set contrast control register
	OLED_WR_Byte(0xCF,OLED_CMD); // Set SEG Output Current Brightness
	OLED_WR_Byte(0xA1,OLED_CMD);//--Set SEG/Column Mapping     0xa0左右反置 0xa1正常
	OLED_WR_Byte(0xC8,OLED_CMD);//Set COM/Row Scan Direction   0xc0上下反置 0xc8正常
	OLED_WR_Byte(0xA6,OLED_CMD);//--set normal display
	OLED_WR_Byte(0xA8,OLED_CMD);//--set multiplex ratio(1 to 64)
	OLED_WR_Byte(0x3f,OLED_CMD);//--1/64 duty
	OLED_WR_Byte(0xD3,OLED_CMD);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
	OLED_WR_Byte(0x00,OLED_CMD);//-not offset
	OLED_WR_Byte(0xd5,OLED_CMD);//--set display clock divide ratio/oscillator frequency
	OLED_WR_Byte(0x80,OLED_CMD);//--set divide ratio, Set Clock as 100 Frames/Sec
	OLED_WR_Byte(0xD9,OLED_CMD);//--set pre-charge period
	OLED_WR_Byte(0xF1,OLED_CMD);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	OLED_WR_Byte(0xDA,OLED_CMD);//--set com pins hardware configuration
	OLED_WR_Byte(0x12,OLED_CMD);
	OLED_WR_Byte(0xDB,OLED_CMD);//--set vcomh
	OLED_WR_Byte(0x40,OLED_CMD);//Set VCOM Deselect Level
	OLED_WR_Byte(0x20,OLED_CMD);//-Set Page Addressing Mode (0x00/0x01/0x02)
	OLED_WR_Byte(0x02,OLED_CMD);//
	OLED_WR_Byte(0x8D,OLED_CMD);//--set Charge Pump enable/disable
	OLED_WR_Byte(0x14,OLED_CMD);//--set(0x10) disable
	OLED_WR_Byte(0xA4,OLED_CMD);// Disable Entire Display On (0xa4/0xa5)
	OLED_WR_Byte(0xA6,OLED_CMD);// Disable Inverse Display On (0xa6/a7)
	OLED_WR_Byte(0xAF,OLED_CMD);//--turn on oled panel

	OLED_WR_Byte(0xAF,OLED_CMD); /*display ON*/
	OLED_Clear();
	OLED_Set_Pos(0,0);
}

//一下提供给波形显示使用
//清空一列
void clearn_alie(char lie)
{
	char i=0;
	//oled_send_cmd(0x2E);//设置页地址

	oled_send_cmd(0x22);//设置页地址
	oled_send_cmd(0);
	oled_send_cmd(7);

	oled_send_cmd(0x21);//设置列起始和结束地址
	oled_send_cmd(lie);
	oled_send_cmd(lie);

	for(;i<8;i++)oled_send_adata(0x00);

}
void display_alie(char lie,char *bmp)
{
	//char i=0;
	//oled_send_cmd(0x2E);//设置页地址

	oled_send_cmd(0x22);//设置页地址
	oled_send_cmd(0);
	oled_send_cmd(7);

	oled_send_cmd(0x21);//设置列起始和结束地址
	oled_send_cmd(lie);
	oled_send_cmd(lie);

	oled_send_datas((uint8_t*)bmp,8);

}

//硬件循环滚动，只能循环移动，可以用来写波形
void hscroll(char y0,char y1)
{
	//滚动模式
	oled_send_cmd(0x27);//滚动方向0x27右，26左
	oled_send_cmd(0x00);//空
	oled_send_cmd(y0);//起始页(range.y0+8)/8
	oled_send_cmd(7);//速度；帧/格 这个是每两帧移动一次，是规格书里最快的了
	oled_send_cmd(y1);//结束页(range.y0+8+range.dy)/8-1
	oled_send_cmd(0);//空
	oled_send_cmd(0xff);//空
	//
//	oled_send_cmd(0x2F);//开始滚
//	oled_send_cmd(0x2E);
}
//硬件停止滚动
void stop_sroll()
{
	oled_send_cmd(0x2E);
}




//#ifdef SH1107
//
//void oled_init(void)
//{
//	oled_i2c_init();
//
//	oled_send_cmd(0XAE);//SET DCDC命令
//	oled_send_cmd(0X8D);//SET DCDC命令
//	oled_send_cmd(0X14);//DCDC ON
//	oled_send_cmd(0XAF);//DISPLAY ON
//	oled_send_cmd(0xD5);//设置显示分频，提高刷新率
//
//	/* Divide ratio= A[3:0] + 1,f:A[7:4] : Range:0000b~1111b */
//	oled_send_cmd(0xF1);//最大的刷新率了
//	oled_send_cmd(0x20);//设置循环显示模式
//	oled_send_cmd(0x00);
//
//	//
//	oled_send_cmd(0xDC);
//	oled_send_cmd(0x00);
//	OLED_Clear();
//
//}
//
//#endif











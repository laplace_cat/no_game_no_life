/*
 * pic_al.h
 *
 *  Created on: 2021年11月29日
 *      Author: 17103
 */

#ifndef COMPONENTS_OLED_INCLUDE_PIC_AL_H_
#define COMPONENTS_OLED_INCLUDE_PIC_AL_H_

#include "oled.h"
#include "port.h"

#define driction
#define UI_RIGHT 0
#define UI_LEFT 1
#define UI_UP 2
#define UI_DOWN 3
//总共会有两个buffer供刷屏使用
//上下左右移动尽量低功耗算法,不是一个个像素点取,而是一次取0-8格,
//这样你如果一次移动两格以上,会相比一个像素移动节省计算量.
//ui结构体就相当于存放图片的框
typedef struct ui_struct{

    int x0; //图片的起始位置（像素）x:0-127
    int y0;//图片的起始位置（像素）y:0-63
    int y8;//0-8移动ui时的y0精细坐标

    int dx;//图片的长（像素）dx
    int dy;
    int len;//图片数组的长度0-1024
    char *bmp;//图片数组的指针

} ui_t;
extern ui_t ui_now;
extern ui_t ui_next;
extern char menu_buffer[1024];
extern char menu_next[1024];

#define put_ui(ui) 		add_ui(ui,ui_now)
#define put_xy(x,y) 	add_pixel(x,y,1,ui_now)
#define clear_buf()   	clearn_sui(ui_now)
//#define draw_buf()		draw_ui(ui_now)

void ui_init(ui_t *ui, char size);
//void ui_init1(ui_t *ui, char x0,char y0,char dx,char dy,const char* bmp);
void ui_init2(ui_t *ui, char x0,char y0, char dx,char dy);

char exchar(char str);
void demoo(void);
void demo2(void);
void demo3(void);
void demo4(void);
void demo_roate();

void mystrcpy(char* dest,const char* souse,int len);
void ui_roll_up(ui_t ui1,ui_t ui2, char n);
void ui_roll_down(ui_t ui1,ui_t ui2, char n);
void ui_roll_right(ui_t ui1,ui_t ui2,char n);
void ui_roll_left(ui_t ui1,ui_t ui2,char n);


ui_t draw_ui(ui_t ui);
void add_ui(ui_t ui,ui_t ground);
void add_bmp(ui_t ui,ui_t blank);
void show_char16(int x0,int y0,char str);
void add_char16(int x,int y,char str,ui_t ground);

void ui_roll_aleft(ui_t ui1,ui_t ui2,float a,char d);
void ui_roll_aright(ui_t ui1,ui_t ui2,float a,char d);
void ui_roll_aup(ui_t ui1,ui_t ui2,float a,char d);
void ui_roll_adown(ui_t ui1,ui_t ui2,float a,char d);

int roate_ui(int angle,ui_t ui1,ui_t ui2);
char get_pixel(int x,int y64,ui_t ui);
char add_pixel(int x,int y64,char value,ui_t ui);
void clearn_sui(ui_t ui);
void ui_y0(ui_t* ui,int y64);
int get_ui_y0(ui_t ui);

void draw_buf();
//int update_y(ui_t*ui);
extern const float sind[361];
extern const float cosd[361];



#endif /* COMPONENTS_OLED_INCLUDE_PIC_AL_H_ */

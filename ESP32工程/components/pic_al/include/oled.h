/*
 * oled.h
 *
 *  Created on: 2021��11��16��
 *      Author: 17103
 */
//#include "sdkconfig.h"

#ifndef OLED_H_
#define OLED_H_

//#include "soft_spi.h"
#include "pic_al.h"

#define SLAVE_ADD 0x78
#define Max_Column	128


void OLED_Clear(void);
void OLED_Init(void);
void OLED_Display_Off();
void OLED_Display_On();
void OLED_Set_Pos(unsigned char x, unsigned char y);
void OLED_DrawBMP(unsigned char x0, unsigned char y0,unsigned char x1, unsigned char y1,unsigned char* BMP);
void clearn_alie(char lie);
void display_alie(char lie,char *bmp);
void hscroll(char y0,char y1);

#endif /* OLED_H_ */

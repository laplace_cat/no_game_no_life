/*
 * bmp.h
 *
 *  Created on: 2021��11��29��
 *      Author: 17103
 */

#ifndef MAIN_BMP_H_
#define MAIN_BMP_H_

extern const char bmp1[1024];
char bmp_buffer[1024];

extern char bmp_mask1[8];
extern char bmp_mask2[8];
extern const char bmp_void[1024];
extern const char bmp_heart[1024];
extern const char bmp_good[288];
extern const char bmp_icontime[1024];
extern const char bmp_heartrate[1024];
extern const char bmp_temphum[1024];
extern const char bmp_ball[1024];
extern const char bmp_step[1024];
extern const char bmp_wifi[1024];
extern const char bmp_foot[1024];
extern const char bmp_heartt[1024];
extern const char bmp_wave_w[1024];

extern const char compass[512];

extern const char bmp_X[32];
extern const char bmp_O[32];
extern const char bmp_Y[32];

extern const char bmp_posses1[256];
extern const char bmp_posses2[256];
extern const char bmp_posses3[256];
extern const char bmp_posses4[256];
extern const char bmp_posses5[256];
extern const char bmp_possesh[256];



extern const char bmp_yezi[512];
extern const char bmp_temp[128];
extern const char bmp_hum[128];

extern const char bmp_num[10][180];
extern const char bmp_num2040[20][100];
extern const char bmp_clock_dev[48];
extern const char bmp_clock_dev4[20];
extern const char bmp_num16[10][20];
extern const char bmp_num_point[4];
extern const char bmp_sun[32];

extern const char bmp_charge_icon[10];
extern const char bmp_wifi_icon[10];
const char bmp_letter16_h[20];
const char bmp_letter16_s[20];
extern const char bmp_front16[];


extern const char bmp_up[1024];
extern const char bmp_down[1024];
extern const char bmp_right[1024];
extern const char bmp_left[1024];


const char bmp_lup[288];
const char bmp_ldown[288];
const char bmp_lright[288];
const char bmp_lleft[288];
extern const char bmp_set1[288];
extern const char bmp_num58[10][5];
extern const char  bmp_str58[97][5];
//struct bmp{
//	const char *s48_up=bmp_lup;
//	const char *s48_down=bmp_ldown;
//	const char *s48_left=bmp_lleft;
//	const char *s48_right=bmp_lright;
//	const char *s48_set=bmp_set1;
//	const char *s48_good=bmp_good;
////	const char *s4848_up=bmp_lup;
////	const char *s4848_up=bmp_lup;
////	const char *s4848_up=bmp_lup;
////	const char *s4848_up=bmp_lup;
//
//}bmps;



#endif /* MAIN_BMP_H_ */

/*
 * roll.c
 *
 *  Created on: 2022年6月12日
 *      Author: 17103
 */
#include "pic_al.h"
#include "bmp.h"
//滚动,滚动是指原来ui的内容会被移动出去,而后面的会移动进来

//从ui1向上滚动到ui2，格数位n
//滚动一64格后ui1内数组会变成原来ui2的数组，ui2会清空
void ui_roll_up(ui_t ui1,ui_t ui2, char n)
{
	int i;
	char temp;
	char temp2[ui2.dx];
	if(n>8)n=8;
	if(n<0)n=0;
	 for(i=0;i < (ui2.len) ;i++)
		 {
		 //提取ui2的第一排
		 if(i<ui2.dx)
			{
				temp2[i]=ui2.bmp[i]&bmp_mask1[n-1];
			}
		 //除第一排以外每一排都要把移除的加回上一排
		 if((i>ui1.dx))
		 	 {
				temp=ui1.bmp[i]&bmp_mask1[n-1];
				ui1.bmp[i-ui1.dx]=ui1.bmp[i-ui1.dx]|(temp<<(8-n));

				temp=ui2.bmp[i]&bmp_mask1[n-1];
				ui2.bmp[i-ui2.dx]=ui2.bmp[i-ui2.dx]|(temp<<(8-n));
			 }
		 //上移n格
		 ui1.bmp[i]=ui1.bmp[i]>>n;
		 ui2.bmp[i]=ui2.bmp[i]>>n;
		 //将UI2第一排移除的数据加在UI1最后一排
		 if(i > (ui1.len-ui1.dx))
		 	 {
			 	 ui1.bmp[i]=ui1.bmp[i]|(temp2[i-ui1.len+ui1.dx]<<(8-n));
		 	 }
		 }
}

//从ui1向下滚动到ui2，格数位n
//滚动后ui1和ui2都会变化
void ui_roll_down(ui_t ui1,ui_t ui2, char n)
{
	int i=0;
	char temp;
	char temp2[ui2.dx];

	if(n>8)n=8;
	if(n<1)n=1;
	//从最后一排开始算
	 for(i=(ui1.len-1);i>=0 ;i--)
		 {
		 	 //UI2最后一排要取出
		 	 if(i >= (ui2.len-ui2.dx) )
		 	 {
		 		 temp2[i+ui2.dx-ui2.len]=ui2.bmp[i]&bmp_mask2[n-1];
		 		 //printf("%d\n",temp2[i+ui2.dx-ui2.len]);
		 	 }
		 	 //向下移动
			 ui1.bmp[i]=ui1.bmp[i]<<n;
			 ui2.bmp[i]=ui2.bmp[i]<<n;

		 	 //将除第一排以外的都加上上一排的数
			 if(i>= ui1.dx)
			 	 {
					temp=ui1.bmp[i-ui1.dx]&bmp_mask2[n-1];
					ui1.bmp[i]=ui1.bmp[i]|(temp>>(8-n));

					temp=ui2.bmp[i-ui2.dx]&bmp_mask2[n-1];
					ui2.bmp[i]=ui2.bmp[i]|(temp>>(8-n));
				 }
			 //将UI2最后一排移除的数据加在UI1第一排
			 if(i < ui1.dx) ui1.bmp[i]=ui1.bmp[i]|(temp2[i]>>(8-n));
		}
}
////从ui1向左滚动到ui2，格数位n
//滚动后ui1和ui2都会变化
void ui_roll_left(ui_t ui1,ui_t ui2,char n)
{
	int i=0;
	int j=0;
	char temp=0;
	if(n>32)n=32;
	if(n<1)n=1;
	//移动n次
	for(;j<n;j++)
	{
		for(i=0;i<ui1.len;i++)
		{
			//当到了第一列，提取ui2第一列的数
			if((i+ui1.dx)%ui1.dx==0) temp=ui2.bmp[i];
			//整体左移
			ui1.bmp[i]=ui1.bmp[i+1];
			ui2.bmp[i]=ui2.bmp[i+1];
			//ui2的第一列放到ui1最后一列
			if((i+1+ui1.dx)% ui1.dx==0) ui1.bmp[i]=temp;
		}
	}
}

////从ui1向左滚动到ui2，格数位n
//滚动后ui1和ui2都会变化
void ui_roll_right(ui_t ui1,ui_t ui2,char n)
{
	int i=0;
	int j=0;
	char temp=0;
	if(n>32)n=32;
	if(n<1)n=1;
	//移动n次
	for(;j<n;j++)
	{
		for(i=ui1.len-1;i>=0;i--)
		{
			//当到了最后一列，提取ui2最后一列的数
			if((i+ui1.dx+1)%ui1.dx==0) temp=ui2.bmp[i];
			//整体左移
			ui1.bmp[i]=ui1.bmp[i-1];
			ui2.bmp[i]=ui2.bmp[i-1];
			//ui2的最后一列放到ui1第一列
			if((i+ui1.dx)% ui1.dx==0) ui1.bmp[i]=temp;
		}
	}
}

//以下为加速度运动,例子在下面两个demo里面
//ui1内容滚出,ui2内容滚入,加速度为a,延时为d
//ui2以加速度a向右边滚动出ui1
void ui_roll_aright(ui_t ui1,ui_t ui2,float a,char d)
{
	char t;
	char distence=0;
	char v=0;
	float temp=0;
	for(t=1;(t<100)&&(distence<ui1.dx);t++)
	{
		if(distence>ui1.dx/2){
			temp=temp-a+0.1;
			v=(char)temp;
		}
		else{
			v=a*t;temp=(float)v;}
		if((distence+v)>ui1.dx)v=ui1.dx-distence;
		//printf("d:%d\n",v);
		ui_roll_right(ui1,ui2,v);
		//OLED_DrawBMP(ui1.x0,ui1.y0,ui1.dx+ui1.x0,ui1.dy+ui1.y0,ui1.bmp);
		draw_ui(ui1);
		distence=distence+v;
		delay_ms(d);
	}
}
//ui2以加速度a向左边滚动出ui1
void ui_roll_aleft(ui_t ui1,ui_t ui2,float a,char d)
{
	char t;
	char distence=0;
	char v=0;
	float temp=0;
	for(t=1;(t<100)&&(distence<ui1.dx);t++)
	{
		if(distence>ui1.dx/2){
			temp=temp-a+0.1;
			v=(char)temp;
		}
		else{
			v=a*t;temp=(float)v;}
		if((distence+v)>ui1.dx)v=ui1.dx-distence;
		//printf("d:%d\n",v);
		ui_roll_left(ui1,ui2,v);
		//OLED_DrawBMP(ui1.x0,ui1.y0,ui1.dx+ui1.x0,ui1.dy+ui1.y0,ui1.bmp);
		draw_ui(ui1);
		distence=distence+v;
		delay_ms(d);
	}
}
//ui2以加速度a向上边滚动出ui1
void ui_roll_aup(ui_t ui1,ui_t ui2,float a,char d)
{
	char t;
	char distence=0;
	char v=0;
	char i;
	float temp=0;
	//a=a+1;
	for(t=1;(t<200)&&(distence<ui2.dy*8);t++)
	{
		//if(v<1)v=1;if(a<1)else v=a*t;v-a*t
		if(distence>ui1.dy*4){
			temp=temp-a+0.1;
			v=(char)temp;
		}
		else{
			v=a*t;temp=(float)v;}
		if((distence+v)>ui1.dy*8)v=ui1.dy*8-distence;

		//printf("d:%d\n",v);
		for(i=v;i>=8;i=i-8){ui_roll_up(ui1,ui2,8);}

		ui_roll_up(ui1,ui2,((v+8)%8));
		//OLED_DrawBMP(ui1.x0,ui1.y0,ui1.dx+ui1.x0,ui1.dy+ui1.y0,ui1.bmp);
		draw_ui(ui1);
		distence=distence+v;
		delay_ms(d);
	}
}
//ui2以加速度a向上下边滚动出ui1
void ui_roll_adown(ui_t ui1,ui_t ui2,float a,char d)
{
	char t;
	char distence=0;
	char v=1;
	char i;
	float temp=0;
	for(t=1;(t<100)&&(distence<ui2.dy*8);t++)
	{
		if(distence>ui1.dy*4){
			temp=temp-a+0.1;
			v=(char)temp;
		}
		else{
			v=a*t;temp=(float)v;}
//		if(distence>ui1.dy*4)v=v-a;
//		else v=a*t;
		if((distence+v)>ui1.dy*8)v=ui1.dy*8-distence;

		//printf("d:%d\n",v);
		for(i=v;i>=8;i=i-8){ui_roll_down(ui1,ui2,8);}

		ui_roll_down(ui1,ui2,((v+8)%8));
		//OLED_DrawBMP(ui1.x0,ui1.y0,ui1.dx+ui1.x0,ui1.dy+ui1.y0,ui1.bmp);
		draw_ui(ui1);
		distence=distence+v;
		delay_ms(d);
	}
}



void demoo(void)
{

	mystrcpy(menu_next, bmp_lup, 288);

	ui_t ui_now;
	ui_init(&ui_now,48);
	ui_t ui_last;
	ui_last=ui_now;
	ui_last.bmp=menu_buffer;
	mystrcpy(menu_buffer, bmp_lright, 288);
	while(1)
	{
		//ui1变成ui2，ui2空(左右移动不会变空)
		//从ui1滚动变成ui2
		ui_roll_aup(ui_last,ui_now,1,10);
//		delay_ms(100);
		//ui1移除成空
		ui_roll_aup(ui_last,ui_now,1,10);
		mystrcpy(menu_next, bmp_lleft, 288);
//		delay_ms(50);

		ui_roll_aleft(ui_last,ui_now,1,10);
//		delay_ms(100);
		mystrcpy(menu_next, bmp_void, 288);
		ui_roll_aleft(ui_last,ui_now,1,10);
		mystrcpy(menu_next, bmp_ldown, 288);
//		delay_ms(50);

		ui_roll_adown(ui_last,ui_now,1,10);
//		delay_ms(100);
		ui_roll_adown(ui_last,ui_now,1,10);
		mystrcpy(menu_next, bmp_lright, 288);
//		delay_ms(50);


		ui_roll_aright(ui_last,ui_now,1,10);
//		delay_ms(100);
		mystrcpy(menu_next, bmp_void, 288);
		ui_roll_aright(ui_last,ui_now,1,10);
		mystrcpy(menu_next, bmp_good, 288);
//		delay_ms(50);

		ui_roll_aright(ui_last,ui_now,1,10);
//		delay_ms(100);
		mystrcpy(menu_next, bmp_void, 288);
		ui_roll_aright(ui_last,ui_now,1,10);
//		delay_ms(50);
		mystrcpy(menu_next, bmp_lup, 288);
	}

}

void demo2()
{
	mystrcpy(menu_next, bmp_posses1, 256);
	mystrcpy(menu_buffer, bmp_posses2, 256);
	ui_t ui_now;
	ui_init2(&ui_now,0,0,128,2);
	ui_t ui_last=ui_now;
	ui_last.bmp=menu_buffer;
		ui_roll_aright(ui_last,ui_now,1,10);
		mystrcpy(menu_next, bmp_posses2, 256);
		ui_roll_aright(ui_last,ui_now,1,10);
		mystrcpy(menu_next, bmp_posses1, 256);
//	int i=0;
//	int temp=0;
//	mystrcpy(menu_next, bmp_lup, 288);
//
//	ui_t ui_now={
//		.x0=0,
//		.y0=1,
//		.dx=47,
//		.dy=47,
//		.bmp=menu_next,
//		.len=288,
//		};
//	ui_t range={
//		.x0=0,
//		.y0=8,
//		.dx=127,
//		.dy=63,
//		};
//	//draw_ui(ui_now);
//	//while(1)
//	{
//		temp=i;
//		for(;i<15;i++)
//		{
//			ui_now.y0=i;
//			draw_ui(ui_now);
//		}
//		ui_now.y0=temp;
//		//clearn_sui(ui_now);
//		ui_now.y0=i;
//		draw_ui(ui_now);
//		delay_ms(50);
//		//hscroll(range,ui_now);
//		//printf("d:%d\n",ui_now.dy);
//		while(1){
//			my_oled_send_cmd(0x2F);//设置页地址
//			delay_ms(8);
//			my_oled_send_cmd(0x2E);//设置页地址
//			clearn_alie(127);
//		}
//
//	}

}

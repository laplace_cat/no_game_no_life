/*
 * pic_al.c
 *
 *  Created on: 2021年11月29日
 *      Author: laplace_cat
 */
#include "port.h"
#include "bmp.h"
#include "oled.h"
#include <string.h>
#include "pic_al.h"
#include "stdio.h"
//#include "front.h"
//由于12864oled一竖向8个像素1个char,上下移动采用比较节省单片机性能的一次移动n格像素(因为只有移动8格才能移动一个char)
//..左右移动则一次八个也就是一个char(移动一格就能移动一个char)

//#define SSD1306
#define SH1107
//#define Y8 //开启定义节省性能

char menu_buffer[1024]={0};
char menu_next[1024]={0};
ui_t ui_now={
	.x0=0,
	.y0=0,
	.dx=128,
	.dy=8,
	.bmp=menu_buffer,
	.len=1024,
	};
ui_t ui_next={
	.x0=0,
	.y0=0,
	.dx=128,
	.dy=8,
	.bmp=menu_next,
	.len=1024,
	};
char buf[1024]={0};

int update_y(ui_t*ui)
{
//	if(ui->y8>0)
//	{
//		ui->y8 =(ui->y0)%8;
//	}else{
//		ui->y8 =(ui->y0+128*2)%8;
//		ui->y8=-ui->y8;
//	}
//	ui->dy=ui->y0/8;
	return ui->y0*8+ui->y8;

}

void clearn_sui(ui_t ui)
{
//	mystrcpy(ui.bmp,bmp_void,ui.len);
	int i;
	for(i=0;i<ui.len;i++)
	{
		ui.bmp[i]=0;
	}
}


//改变y0值,单位不是页而,是像素
void ui_y0(ui_t* ui,int y64)
{
	if(y64<0){
		y64=-y64;
		ui->y8=-(y64%8);
		ui->y0=-(y64/8);
	}else{
		ui->y8=y64%8;
		ui->y0=y64/8;
	}
}
int get_ui_y0(ui_t ui)
{
	int y=ui.y0*8+ui.y8;
	return y;
}

void ui_xy64(ui_t* ui,int x,int y64)
{
	ui->x0=x;
	ui->y8=y64%8;
	ui->y0=y64/8;
}
void show_num8(int x0,int y0,const char *str,int len)
{
//	ui_t ui;
//	ui_init(&ui,8);
//	ui.dx=5;
	int i=0;
	int x=x0;
	char c=0;
	for(;(x<128)&&(i<len)&&(str[i]!='\0');i++)
	{
		c=str[i]-48;
		//show_char16(x,y0,c);
		mystrcpy(ui_now.bmp+x,(const char *)bmp_num58+c*5,5);
		OLED_DrawBMP(x,y0,x+5,y0+1,(unsigned char *)bmp_num58+c*5);//+ui_now.dx
		x=x+5;
		//printf("%d\n",c);
	}
}

#ifdef SH1107
#define FLAME_TEST 10
ui_t draw_ui(ui_t ui)
{
	static char str[10]={0};
	static unsigned long long t64last;
	static int times=FLAME_TEST;
	unsigned long long temp=20;
	times--;
	if(times<0)
	{
		temp=get_timer0()-t64last;
		times=FLAME_TEST;
		t64last+=temp;
		//printf("fps:%f\n",60000/(double)temp);
		sprintf(str,"%d",(int)(6000*FLAME_TEST/temp));
	}
	show_num8(118,0,str,3);
	OLED_DrawBMP(ui.x0,ui.y0,ui.dx+ui.x0,ui.dy+ui.y0,(unsigned char *)ui.bmp);
	return ui;
}
#endif


//适合不带y8值的
void add_bmp(ui_t ui,ui_t blank)
{
	int y;
	int p;
	for(y=0;y<ui.dy;y++)
	{
		p=ui.y0*blank.dx+ui.x0+y*blank.dx;
		mystrcpy(blank.bmp+p,ui.bmp+y*ui.dx,ui.dx);
	}
}
//默认使用next作为缓冲区,放心使用
void ui_init(ui_t *ui, char size)
{
	ui->x0=0;
	ui->y0=0;
	ui->dx=size;
	ui->dy=size/8;
	ui->len=size/8*size;
	ui->bmp=ui_next.bmp;
	ui->y8=0;
}
void ui_init2(ui_t *ui, char x0,char y0, char dx,char dy)
{
	ui->x0=x0;
	ui->y0=y0;
	ui->dx=dx;
	ui->dy=dy;
	ui->len=dx*dy;
	ui->bmp=ui_next.bmp;
	ui->y8=0;
}




void mystrcpy(char* dest,const char* souse,int len)
{
	memcpy(dest,souse,len);
}


//
////任意方向移动图标,移动注意是移动!!
////把ui放在背景里面动
////将ui移动s格后放在blackground上,ui不变
////ui的图片指针可以是常量,blackground的图片指针是变量
////没啥用的函数,直接改y64值然后add后画就行了
//void ui_move(ui_t* ui,ui_t ground,int distence,char driction)
//{
//	int i=0;
//	for(;i<distence;i++)
//	{
//		switch(driction)
//		{
//			case 0:ui->x0++;break;
//			case 1:ui->x0--;break;
//			case 2:ui_y0(&ui,ui->y0*8+ui->y8+1);break;
//			case 3:ui_y0(&ui,ui->y0*8+ui->y8-1);break;
//		}
//		add_ui(*ui,ground);
//		draw_ui(ground);
//	}
//}
//
////加速度运动
////x,y任意
//void ui_amove(const ui_t ui,ui_t blackground,char driction,float a,char delay)
//{
//	int s=0;
//	int t=1;
//	int size=1;
//	//ui_t temp=blackground;
//	switch(driction)
//	{
//		case 0:size=blackground.dx;break;
//		case 1:size=blackground.dx;break;
//		case 2:size=blackground.dy;break;
//		case 3:size=blackground.dy;break;
//
//	}
//	for(;s < size;t++)
//	{
//		s=t*t*a/20;
//		//ui_roll(ui,blackground,s,driction);
//
//		//printf("s:%d\n",s);
//		delay_ms(50);
//		draw_ui(blackground);
//	}
//}

//把ui放到ui背景里,将ui放到ground的x,y位置上
//比较消耗资源
void add_ui(ui_t ui,ui_t ground)//
{
	int x,y;
	char value;
	for(x=0;x<ui.dx;x++)
	{
		for(y=0;y < ui.dy*8;y++)
		{
			value=get_pixel(x,y,ui);//printf("d;%d\n",value);
			//if(value==1)
			add_pixel(x+ui.x0,get_ui_y0(ui)+y,value,ground);

		}
	}
	//printf("y0:%d\n",get_ui_y0(ui));//ui.y0*8+ui.y8
}

#define FPS_FLAME 10
void draw_buf()
{

//	add_ui(ui_t ui,ui_t ground)
	draw_ui(ui_now);


}



//已经用define实现
//把ui放在buffer上,不进行绘制,能绘制-127...255,-63...127像素任意位置
//void put_uifer(ui_t ui)
//{
//	add_ui(ui,ui_now);
//}


//极限性能测试
//put_ui是极其耗费性能的
void demo4()
{
	//mystrcpy(menu_next, bmp_lup, 288);
	ui_t ui;
	ui_init(&ui,48);
	ui.bmp=bmp_lup;
	ui_t uid;
	ui_init(&uid,48);
	uid.bmp=bmp_ldown;
	//uid.x0=48;

	int y=-63;
	while(1)
	{
//	y=-63;
//	for(;y<63;y++)
//	{
//		//要是有cpp的运算符重载就好了,但是为了兼容嘛
//		ui_y0(&ui,-y);
//		ui_y0(&uid,y);
//		clear_buf();
//		ui.x0=42;
//		put_ui(ui);
//		ui.x0=42+42;
//		put_ui(ui);
//
//		ui.x0=0;
//		put_ui(ui);
//
//		uid.x0=42;
//		put_ui(uid);
//		uid.x0=42+42;
//		put_ui(uid);
//		uid.x0=0;
//		put_ui(uid);
//
//		//draw_ui(ui_now);
//		draw_buf();
//	}
	ui.y0=0;
	ui.y8=0;
	ui.x0=-48;
	for(;ui.x0<128;ui.x0++)
	{
//		clear_buf();
		put_ui(ui);
		draw_buf();
	}
	}
}


//



//
//void demo3()
//{
//	int i=0;
//	int j=0;
//	int temp;
//	int n;
//	int *display;
//
//	int dis_y[128];
//	blood_message play_data;
//
//
//		if(bloodq!=NULL)
//		{
//		if( xQueueReceive( bloodq, &play_data, ( TickType_t ) 10 ))
//			{
//				display=play_data.red_message;
//				//printf("get a data\n");
////				for(i=0;i<DISPLAY_BUFFER;i++)
////					{printf("rdata:%d\n",play_data.red_message[i]);delay_ms(1);}
//				for(i=0;i<DISPLAY_BUFFER;i++)
//				{
//					display[i]=display[i]-play_data.red_dc;
//				}
//				temp=find_max(display,DISPLAY_BUFFER)-find_min(display,DISPLAY_BUFFER);
//				for(i=0;i<DISPLAY_BUFFER;i++)
//				{
//					//printf("d:%d\n",display[i]);
//					display[i]=display[i]+temp;
//				}
//				//整体上移
//				//temp=temp/102;
//				//printf("d:%d\n",temp);
//				for(i=0;i<DISPLAY_BUFFER;i++)
//				{
//					if(temp>=0&&temp<10000)
//					{
//						display[i]=display[i]*102/temp;
//					}
//					//printf("a:%d,t:%d\n",display[i],temp);
//				}
//				n=4;
//				//y轴变换
//				for(i=0;i<(DISPLAY_BUFFER/n);i++)
//				{
//					for(j=0;j<n;j++)
//					{
//						display[i]+=display[i*n+j];//display[i]=display[i]/n;
//						//printf("d:%d\n",display[i]);
//					}
//				}
//				display_wave(display);//显示波形
//			}
//		}
//}

//斜向移动，不能运行
//void ui_roll_upright(ui_t ui1,ui_t ui2,char n)
//{
//	ui_roll_up(ui1,ui2,n);
//	ui_roll_right(ui1,ui2,n*2);
//}
//
//void ui_roll_upleft(ui_t ui1,ui_t ui2,char n)
//{
//	ui_roll_up(ui1,ui2,n);
//	ui_roll_left(ui1,ui2,n*2);
//}
//void ui_roll_downright(ui_t ui1,ui_t ui2,char n)
//{
//	ui_roll_down(ui1,ui2,n);
//	ui_roll_right(ui1,ui2,n*2);
//}
//void ui_roll_downleft(ui_t ui1,ui_t ui2,char n)
//{
//	ui_roll_down(ui1,ui2,n);
//	ui_roll_left(ui1,ui2,n*2);
//}

////反转一个char
//char exchar(char str)
//{
//	char temp=0;
//	char i;
//	for(i=0;i<4;i++)
//	{
//		 temp=temp|( (str&(0x01<<i)) << (7-2*i));
//	}
//	for(i=4;i<8;i++)
//	{
//		 temp=temp|((str&(0x01<<i)) >> (2*i-7));
//	}
//	return temp;
//}


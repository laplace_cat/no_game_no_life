#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "stdio.h"
#include "game.h"
#include "port.h"
void app_main(void)
{
	 //wifi_init_sta();
	io_init_io();
	 wifi_connect();
	int key=0;
	eap32_timer00_init();
	oled_gpio_init();
	key_q_init();
	//非常奢侈的轮询检测按键,固定在小核上
	xTaskCreatePinnedToCore(serial_key_task, "serial_key_task", 1000, "task_first",2, NULL, 0);
	//xTaskCreatePinnedToCore(tcp_client_task, "tcp", 4096, "tcp",3, NULL, 0);
	xTaskCreatePinnedToCore(game, "game", 4096, "game",1, NULL, 0);

    while (true) {
    	//serial_key(&key);
    	//snake();
    	vTaskDelay(1);
    }
}

